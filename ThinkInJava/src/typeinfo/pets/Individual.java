package typeinfo.pets;

/**
 * Created by liupf on 2017/4/27.
 */
public class Individual {
    private static int count = 0;

    private String name;

    public Individual(String name) {
        this.name = name;
    }

    public Individual() {
    }

    public int id() {
        return ++count;
    }

    public String toString() {
        return name.isEmpty() ? this.getClass().getSimpleName() : name;
    }
}
