package typeinfo.pets;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by liupf on 2017/4/28.
 */
public class ForNameCreator extends PetCreator {
    private static String[] allTypes = {"typeinfo.pets.Cat",
            "typeinfo.pets.Cymric",
            "typeinfo.pets.Dog",
            "typeinfo.pets.EgyptianMau",
            "typeinfo.pets.Hamster",
            "typeinfo.pets.Manx",
            "typeinfo.pets.Mouse",
            "typeinfo.pets.Mutt",
    };

    private static List<Class<? extends Pet>> types = new ArrayList<Class<? extends Pet>>();

    static {
        load();
    }

    @SuppressWarnings("unchecked")
    private static void load() {
        for (String type : allTypes) {
            try {
                types.add((Class<? extends Pet>) Class.forName(type));
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public List<Class<? extends Pet>> getTypes() {
        return types;
    }
}
