package typeinfo.pets;

import java.util.HashMap;

/**
 * Created by liupf on 2017/4/28.
 * count numbers of different pets
 */
public class PetCount {
    static class PetCounter extends HashMap<String, Integer> {
        public void count(String type) {
            Integer numbers = get(type);
            if (numbers == null) {
                put(type, 1);
            } else {
                put(type, ++numbers);
            }
        }
    }

    public static void countPets(PetCreator creator) {
        PetCounter counter = new PetCounter();
        for (Pet pet : creator.arrayList(20)) {
            if (pet instanceof Pet) {
                counter.count("Pet");
            }
            if (pet instanceof Cat) {
                counter.count("Cat");
            }
            if (pet instanceof Dog) {
                counter.count("Dog");
            }
            if (pet instanceof Mouse) {
                counter.count("Mouse");
            }
        }
        System.out.println(counter);
    }

    public static void main(String[] args) {
        countPets(new ForNameCreator());
    }
}
