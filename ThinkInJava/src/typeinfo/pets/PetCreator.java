package typeinfo.pets;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * Created by liupf on 2017/4/28.
 * create different classes of Pet randomly
 */
public abstract class PetCreator {
    public abstract List<Class<? extends Pet>> getTypes();

    private static Random rand = new Random(47);

    private Pet create() {
        try {
            return getTypes().get(rand.nextInt(getTypes().size())).newInstance();
        } catch (InstantiationException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    public Pet[] createArray(int size) {
        Pet[] pets = new Pet[size];
        for (int i = 0; i < size; i++) {
            pets[i] = create();
        }
        return pets;
    }

    public ArrayList<Pet> arrayList(int size) {
        ArrayList<Pet> result = new ArrayList<Pet>();
        Collections.addAll(result, createArray(size));
        return result;
    }

}
